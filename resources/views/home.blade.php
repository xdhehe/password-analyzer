<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Password Analyzer</title>
    <!-- BS4 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <style>
        html, body {
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
        }

        .title {
            font-size: 40px;
        }
    </style>
</head>

<body class="container">
    <div class="card mt-5">
        <div class="title mt-5">
            <center>Password Analyzer</center>
        </div>

        <div class="row mt-2">
            <div class="col-md-6">
                <h5 class="ml-5"><b>Characteristics:</b></h5>
                <p class="ml-5">
                   ● Must be at least 8 characters long<br>
                   ● Must have letters and at least one number or symbol<br>
                   ● Must have both upper and lower case letters<br>
                </p>

                <h5 class="ml-5"><b>Other Format Rules:</b></h5>
                <p class="ml-5">
                    ● No Space Allowed<br>
                    ● Maximum Password length is 30<br>
                </p>
            </div><!-- end of col -->
            <div class="col-md-6">
                <h5 class="ml-5"><b>The Result will be determined by:</b></h5>
                <p class="ml-5">
                   ● Strong password - has all of the following characteristics<br>
                   ● Good - has two of the characteristics<br>
                   ● Not Bad - has only one characteristics<br>
                   ● Weak - doesn’t meet any of the listed characteristics<br>
                   ● Duplicate - the password already exists in the database
                </p>
            </div><!-- end of col -->

        </div><!-- end of row-->

        <form method="post" action="{{action('PasswordController@checkingInput')}}" class="mt-3">
        @csrf
            <center>
                <div class="input-group w-50">
                  <input type="text" class="form-control" name="password" placeholder="Your Password" pattern="[^\s]+" minlength="1" maxlength="30">
                  <div class="input-group-append">
                    <button class="btn btn-success font-weight-bold" type="submit">Submit</button>
                  </div>
                </div>

                @if(!empty($result))
                    <h4 class="mt-4">
                        Result:
                        <b>{{$result}}</b>
                    </h4>
                @endif
            </center>
        </form><br><br>

    </div><!-- end of div Card -->
</body>
</html>


