<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Password;

class PasswordController extends Controller
{
    public function home(){
    	return view('home');
    }

    public function checkingInput(Request $request){
    	$this->validate($request,[
            'password' => 'required',
        ]);

        $input = $request->input('password');
        $result = $this->checkingDuplicate($input);

        if($result != "Duplicate"){
            $char1 = $this->checkingFirstChar($input);
            $char2 = $this->checkingSecondChar($input);
            $char3 = $this->checkingThirdChar($input);
            

            $total = $char1 + $char2 + $char3;
            $result = $this->checkingResult($total);
            $this->store($input);

            return view('home')->with('result',$result);
        }
        else
            return view('home')->with('result',$result);
        
    }

    public function checkingDuplicate($input){
		if (Password::where('password', '=', $input)->exists())
			$result = "Duplicate";
		else
			$result = "";

		return $result;
    }    

    public function checkingFirstChar($input){
        if(preg_match('/.{8,}/', $input))
            return 1;
        else
            return 0;
    }

    public function checkingSecondChar($input){
        if (preg_match('/\w{2,}/', $input) && (preg_match('/[^\d^\w]+/', $input) || preg_match('/\d+/', $input)))
            return 1;
        else
            return 0;   
    }

    public function checkingThirdChar($input){
        if(preg_match('/[a-z]/', $input) && (preg_match('/[A-Z]/', $input)))
            return 1;
        else
            return 0;
    }


    public function checkingResult($checkResult){
    	if($checkResult == 3)
    		$result = "Strong";
    	elseif ($checkResult == 2) {
    		$result = "Good";
    	}
    	elseif ($checkResult == 1) {
    		$result = "Not Bad";
    	}
    	elseif ($checkResult == 0) {
    		$result = "Weak";
    	}
		return $result;
    }

    public function store($input){
   	 	$passwordDb = new Password;
        $passwordDb->password = $input;
        $passwordDb->save();
    }

}